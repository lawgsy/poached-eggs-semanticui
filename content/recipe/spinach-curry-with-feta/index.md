---
title: "Spinach Curry With Feta"
date: 2021-06-17T12:31:19+02:00
draft: false
tags: ["curry",  "spinach", "feta", "tomato", "rice", "pita", "naan"] # ingredients, keywords
categories: ["Dinner"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

Mild, creamy "curry" with tomato and feta, best served with rice and bread (such as naan or pita).

<!--more-->

# Notes
* `Serves 3-4`
* Cooking time is `roughly 15-25 minutes`, depending on how fast you can thaw the spinach, heat through the curry itself, cook the rice, bake (off) the bread, cut stuff, etc.

# Requirements
* Large pan
* Something to prepare the naan/pita as mentioned on the packaging, or bake from scratch

# Ingredients
* `750g+ Frozen Spinach à la Crème`, can be substituted with regular frozen spinach (just `add Crème Fraîche`)
* `2-3 Onions`, cut into 6 to 8 chunks
* `2 Beefsteak Tomatoes`, diced
* `~200g Feta` or a substitute: `White Cheese, Salad Cheese...`
* `2-3tbsp Cheap Curry Powder`
* `~300g Rice` (about 100-120g per person)
* `Naan Bread` (at least one per person)
* `1 Cclove of Garlic`, minced (optional)
* `Olive oil`
* Salt and pepper to taste

# Method
1. Cook rice according to packaging.
2. Heat some `olive oil in the pan`, `sautè the onion chunks` until translucent.
3. Add `minced garlic`, `tomatoes` and `curry powder`, heat through.
4. Add all of the `spinach (à la crème)`, keep stirring occasionally until completely thawed and then let it warm through very well.
5. Turn off heat, dice the `'feta' cheese` and add it to the mixture.
6. Toast/bake bread according to packaging.
7. Serve with rice and bread.

# Sources
* Based on [[Spi­na­zie­cur­ry met kaas en naan­brood|https://www.ah.nl/allerhande/recept/R-R559646/spinaziecurry-met-kaas-en-naanbrood]] by Albert Heijn
