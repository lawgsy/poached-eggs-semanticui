---
title: "Rhubarb and Tomato Soup"
date: 2021-06-17T14:01:42+02:00
draft: false
tags: ["rhubarb", "tomato", "soup"] # ingredients, keywords
categories: ["Dinner", "Soup", "Summer"]
servings: "4"
lang: "en"
---

This soup can be made as simple or complex as you like by for example sautéing an onion and some garlic before adding the broth, adding some dried herbs during the process or fresh ones for garnish. But honestly, it doesn't really need any of that to be highly enjoyable.

<!--more-->

# Notes
* `Serves 2-3 as main meal` (serve with some bread perhaps), or `3-4 as side dish`
* Vegan

# Ingredients
* `2L of Vegetable Broth` (from stock cubes)
* `300-500g of Rhubarb stems` (leaves are inedible)
* `400g Can of Diced Tomatoes` (with juice)
* `Sugar` (or Honey), to taste (optional)
* `Salt & Pepper`, to taste

# Method
1. `Bring broth to a boil`
2. Add `rhubarb and tomatoes`
3. `Cook until rhubarb is soft`, this should happen pretty quickly
4. `Blend if necessary` (it probably is not), using an immersion blender
5. (optional) `Add a tablespoon or so of sugar` to take the edge off the sourness of the rhubarb (and tomatoes)
6. Serve while hot
