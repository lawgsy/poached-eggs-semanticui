---
title: "Beetroot and Lamb's Lettuce Salad"
date: 2021-06-13T20:46:19+02:00
draft: false
tags: ["pasta", "salad", "beet", "lambs lettuce", "pine nut", "apple"] # ingredients, keywords
categories: ["Dinner", "summer"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

A nice summer salad with beetroot and lamb's lettuce.

Lamb's lettuce is also known as (common) cornsalad, mâche, fetticus, feldsalat,
nut lettuce and field salad. In German-speaking Switzerland it is known as
Nüsslisalat or Nüssler, terms that have been borrowed by the area's many English
speakers. In some areas of Germany it is known as rapunzel, and is the origin of
the long-haired maiden's name in the eponymous fairy tale.

<!--more-->

## Notes
* `Serves 3-4`
* Contains `gluten` unless gluten-free pasta is used

## Ingredients
* `300g Pasta` (Any will do, but penne, rigato or rigatoni work great)
* `1 Onion`, finely minced
* `75-80g Lamb's lettuce`, washed
* `100-200g Bacon (or bacon substitute)`, in small pieces
* `200mL Crème Fraîche`
* `400-500g Beetroots`
* `2 Sweet Apples`, big ones will do
* `1 handful of pine nuts`, or about 50g

## Method
1. `Cook pasta` according to package instructions
   * Optionally, after cooking, hold under running cold water so it won't stick (it needs to be cold for the salad anyway) - this will remove the starch.
2. `Prepare beetroots`
   * Method 1: Cook, peel, then dice
      ** ''Note on this method:'' if you cook without cutting an end off, the most nutrients and flavours are retained
   * Method 2: Peel, cook, then dice
3. Dry roast (in a frying pan)
   3.1 `Pine nuts` until golden brown
   3.2 `(Substitute) Bacon` until brown/dark to bring out the flavour
4. `Add onion, beetroots, pine nuts and bacon (substitute) to pasta`
5. Serve
