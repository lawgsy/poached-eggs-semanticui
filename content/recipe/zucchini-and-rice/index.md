---
title: "Zucchini and Rice"
date: 2021-06-17T12:53:43+02:00
draft: false
tags: ["zucchini", "rice", "tomato", "dill", "parsley", "onion", "black olives", "yoghurt"] # ingredients, keywords
categories: ["Dinner"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

A recipe from the Balkans with influences from Italy.

The cooking can be cut in nearly half by omitting putting it into the oven. In that case it basically turns into a one pot dish.

<!--more-->

# General information
* `Serves 3-4 people` (~6 to 8 servings)
* Cooking time is `~40-60 minutes` (or ~85-105 minutes, if you also use the oven)

# Ingredients
* `3 Onions`, diced
* `3 cloves of Garlic` (optional), finely chopped or pressed
* `400g canned, diced Tomatoes`
* `1kg Zucchinis`, sliced into chunks of roughly 1cm wide
* `2tbsp Dill`
* `2tbsp Parsley`
* `200g Risotto or round grain Rice` (or more if preferred)
* `650-750mL of broth` (vegetable or chicken) - add more water while cooking if you've added more rice (to get a risotto like consistency), before throwing it into the oven
* `4tbsp of Olive Oil`
* `Chili powder`, to taste
* `Salt and Pepper`, to taste
* Some `Dill branches` and `Black Olives` to garnish
* `Thick Bulgarian (or Turkish) Yoghurt`, to serve with it

# Method
## Without oven:
1. Sauté `onions`, adding the `garlic` shortly after (stir and make sure it doesn't burn or brown) in half of the `olive oil`.
2. Add the `chili powder` and `tomatoes` and let it simmer for `5 to 8 minutes`.
3. Add the `zucchinis` and a bit of salt, mix well and let it cook gently for another `10 to 15 minutes`.
4. Add the `rice`, mix well, add the `broth`  and let it simmer until the rice is done (anything from 20-45 minutes, depending on the rice, amount of liquid and how hard it simmers).
5. Take pan off the heat, mix in the `dill` and `parsley`. Spread the rest of the `olive oil` over the top.
6. `Garnish with dill and olives`, serve `yoghurt` separately

## With oven:
1. Sauté `onions`, adding the `garlic` shortly after (stir and make sure it doesn't burn or brown) in some `olive oil`.
2. Add the `chili powder` and `tomatoes` and let it simmer for `5 to 8 minutes`.
3. Add the `zucchinis` and a bit of salt, mix well and let it cook gently for another `10 to 15 minutes`.
4. Preheat `oven to 190°C (374°F)`
5. Add the `rice`, mix well, add the `broth`  and let it simmer until the rice is done (anything from 20-45 minutes, depending on the rice, amount of liquid and how hard it simmers).
6. Take pan off the heat, sprinkle with `dill` and `parsley`.
7. Transfer pan content to `oven dish` and throw it into the oven for `45 minutes`. Half way through, spread some `olive oil` over the top.
8. `Garnish with dill and olives`, serve `yoghurt` separately

# Notes
* This dish tastes even better cold (say the next day, out of the fridge).

# Source(s)
* __The Practical Encyclopedia of East European Cooking__ by `Lesley Chamberlain` (ISBN: 90 5426 110 2)
