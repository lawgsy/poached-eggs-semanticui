---
title: "Leek Tarte Tatin"
date: 2021-06-17T15:53:52+02:00
draft: false
tags: ["leek", "bacon", "cheese", "cremaceto", "puff pastry"] # ingredients, keywords
categories: ["Dinner", "Oven"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

Description here

<!--more-->

# Notes
* `Serves 3-4`, but will require something on the side such as rice

# Ingredients
* `500g Leek`
* `3-4tbsp Cremaceto` (100g Balsamic + 50g sugar boiled down to a syrup)
* `100g (Vegetarian) Smoked Bacon`, You can also use regular bacon/vegetarian replacement and add some smoked paprika powder.
* `100g Grated Cheese`, for example Old Gouda
* `270g Fresh Puff Pastry`, or use a package from the freezer
* `2tbsp French Mustard`

# Method
1. `Preheat the oven on 200°C (392°F)`
1. `Spread Cremaceto` on the bottom of a springform pan (make sure it seals properly or you will have Cremaceto all over your oven)
1. `Clean the leek(s) and slice them in rings of about one centimeter` thick
    * The easiest way to clean them is slicing them lengthwise, almost but not completely through and then separating the layers slightly while under a running tap. [Like this, for example](https://www.simplyrecipes.com/recipes/how_to_clean_leeks/).
    * You get the nicest effect if the rings stay intact
    * Don't throw away the dark green bit (unless it doesn't look appetizing). If it doesn't fit in the pie, you can always just boil them to serve on the side.
1. `Line the pan with leekrings`, moving inwards from the edge, making sure they are tightly packed
    * I like to try and get coloured rings by 'spiralling' towards the center
1. `Spread a layer of bacon pieces (or replacement)`, then a layer of grated cheese
1. `Smear a layer of mustard onto the pastry`, in a circle about the size of the pan.
1.` Cover the pan with the pastry`, mustard size down and 'tuck in' the leek by pushing down the sides.
1. Using a fork, `puncture about 15 holes into the pastry`.
1. `Bake 30 minutes just below the middle of the preheated oven`
1. Let it rest outside of the oven for about 5 minutes.
1. Cut along the edge of the pan to ensure the pie will neatly come clean out of the pan.
1. Put a large plate on top of the pan and flip.
1. Serve with rice

# Sources
* [Albert Heijn - Hartige omgekeerde preitaart met spekjes (tarte tatin)](https://www.ah.nl/allerhande/recept/R-R1193369/hartige-omgekeerde-preitaart-tarte-tatin)
