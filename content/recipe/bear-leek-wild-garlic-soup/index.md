---
title: "Bear Leek (Wild Garlic) Soup"
date: 2021-06-17T13:11:53+02:00
draft: false
tags: ["bear leek", "crème fraîche", "soup"] # ingredients, keywords
categories: ["Dinner", "Soup", "Spring"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

An easy, creamy bear leek (`daslook` in Dutch: badger leek, `bärlauch` in
German: bear leek) soup with mushroom undertones. Though bear leek is only
available for a short period of time, this soup can be stored in the freezer for
up to about 3 months.

Also known as wild garlic, ramsons, buckrams, broad-leaved garlic, wood garlic,
bear's garlic, bear leek is a wild relative of onion, native to Europe and Asia,
where it grows in moist woodland. It is said to fend off bears, moles and
possibly other woodland creatures (badgers?).

<!--more-->

# Notes
* Bear leek is `very similar in appearance to` two other `poisonous plants`.
  * While this plant occasionally gets sold in certain market stalls in certain countries, if you have never harvested or even seen this plant, please use caution.
    From [wikipedia](https://en.wikipedia.org/wiki/Allium_ursinum):

    >>> In Europe, where ramsons are popularly harvested from the wild, people are regularly poisoned after mistakenly picking lily of the valley or Colchicum autumnale.
* In the Netherlands, this plant is a protected species. You are not allowed to harvest it in the wild there (we can only do so because it grows in our own garden and in major quantities, like a weed).
* `Serves 2-3 as main meal` (serve with some bread perhaps), or `3-4 as side dish`

# Ingredients
* `2L of (wild) Mushroom broth` (from stock cubes)
  * Any broth will do, but mushroom complements the bear leek.
* `1 Onion`, minced
* `1 clove of Garlic`, minced
* `200mL Crème Fraîche` (or a replacement such as heavy cream)
* `1-2 large handfuls of Bear Leek leaves`, washed and finely chopped
  * If you do not chop, you will not be able to use an immersion blender. It has a very high fiber content, and I for one have already blown up one immersion blender by trying to do it the lazy way.
  * The bulbs and flowers are also edible

# Method
1. Let the `Crème Fraîche` sit at room temperature so it will not curdle. If it happens to curdle later, you can blend the final result.
   * Dairy products with higher fat content, such as whipping cream and heavy cream, are less prone to curdling.
2. `Sauté minced onions` until soft and translucent, making sure they don't burn
3. Add the `minced garlic and sauté` a little more
4. Add `broth`, then bring to a rolling boil
5. Add the `chopped up Bear Leek`
6. Add the `Crème Fraîche`
7. Serve while hot
