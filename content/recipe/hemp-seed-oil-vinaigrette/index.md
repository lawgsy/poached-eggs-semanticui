---
title: "Hemp Seed Oil Vinaigrette"
date: 2021-06-17T14:21:18+02:00
draft: false
tags: ["hempseed", "vinegar", "salad", "vinaigrette", "dressing", "lemon", "lime", "mustard"] # ingredients, keywords
categories: ["Dressing"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

Just a simple salad dressing.

<!--more-->

# Ingredients
* `3tbsp Hempseed Oil`
* `1tbsp (Balsamic) Vinegar`
* `1tbsp Lemon Juice`, or `lime`
* `½ to 1 tbsp of (Dijon) Mustard`
* `Pinch of Salt`, to taste
* `Black Pepper`, to taste

# Method
2. Whisk together ingredients
3. Adjust salt and pepper to taste.

# Sources
[[https://www.thespruceeats.com/easy-hempseed-oil-vinaigrette-salad-dressing-3377583]]
