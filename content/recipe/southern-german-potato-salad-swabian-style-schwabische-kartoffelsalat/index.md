---
title: "Southern German Potato Salad, Swabian Style (Schwäbische Kartoffelsalat)"
# subtitle: "Swabian Style (Schwäbische Kartoffelsalat)"
date: 2021-06-12T19:41:38+02:00
draft: false
tags: ["potato", "German", "Swabian", "kartoffelsalat"] # ingredients, keywords
categories: ["Salad", "Dinner"] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

German potato salad originating from the south of Germany (Swabia, to be precise). Delicious in the summer (and winter...and spring...and autumn).

<!--more-->

# Ingredients
* `1kg Potatoes of similar size` (not too large, non-crumble, sturdy, buttery ones if possible)
* `0.25L of water` or broth
* `1 Medium (Yellow) Onion`, finely minced
* `¾ to 1 tbsp Salt (about 13 to 17g)`
* `4 tbsp Vinegar (about 20mL)` (not too dark, eg. white vinegar)
* `2 tbsp Oil (about 10mL)` (neutral-tasting, eg. sunflower)
* `(White) Pepper` (black will do, too)

# Method
1. `Steam the potatoes` (in a steam basket/pan if possible. Otherwise cook in small layer of water. You can also simply boil them, but they may fall apart more easily). Make sure they are nearly done, not well-done, as they will continue to cook slightly.
   * **Note**: A good alternative is to add the potatoes to a pan and put them **just** about underwater. Then bring to a rolling boil and turn off the heat, testing them with a fork occasionally
2. Leave them on a plate/cutting board until cool enough to peel, then `peel them`
   * **Note**: Once cooled, they also tend to be less crumbly
3. Add the `broth/water, salt, vinegar, oil and a dash of pepper` into a large bowl and mix it together.
   * **Note**: It is wise not to add the all of the salt straight away, it depends on the type of salt how strong it will taste
4. Taste, it should have a pleasant dressing-type taste - slightly sour, slightly salty, not too much of both
   * **Note**: Add a little bit of water and/or oil if necessary
5. Add the `minced onions`
6. Add the `potato` by slicing them (say 3mm thick) into the bowl
7. Add `pepper` to taste.
8. Serve (at room temperature - if taken from the fridge take out about 30 minutes early)

Extra tips:
* Let it soak in the flavours for as long as possible. Occasionally toss. Store refrigerated, you can easily make it a day or even two before eating it. The longer the better, actually.
* The potato salad is always prepared a little earlier, so the flavour gets sucked up by the potatoes. You never serve it completely cold, but you can warm it up a little if necessary.
* If you taste the dressing, it should be well-balanced. The quantities may seem large, but they cancel eachother out. You can always add more salt or more vinegar or oil, but definitely do not overdo it. Try to stick to the given ratio.

# Sources
* Die Deutsche Küche, p. 367, recipe #37
