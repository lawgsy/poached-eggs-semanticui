---
title: "Creamy Mushroom Risotto-Style Pasta"
date: 2021-06-12T18:08:56+02:00
draft: false
tags: ["mushroom", "pasta"]
categories: ["Dinner"]
servings: 4
lang: "en"
---

A creamy, mushroomy, risotto-style pasta. Rich in flavour yet an easy essentially one-pot dish. Perfect for autumn due to the mushrooms being used, can be made in all seasons and with different types of mushrooms.

<!--more-->

# Notes
* `Serves 3-4`

# Ingredients
* `400-500g (Button) Mushrooms`
* `2 Shallots` or `2 Small Onions`, minced
* `500g Pasta` (Any will do, but penne, rigato or rigatoni work great - the more surface area/cavities/ribs they have, the more sauce can adhere to it)
* `1½ to 2L of (Mushroom) Broth`
* `250mL Crème Fraîche`
* `1 Clove of Garlic`, minced

# Method
1. Let `crème fraîche` sit out of the fridge to become closer to room temperature
2. Clean and slice the `mushrooms`
3. Create the broth
   * Note: Clean the mushrooms with a (damp) paper towel, or a brush, letting them soak in water will take its toll on the flavour
4. `Sauté onions` until translucent
5. `Add the garlic`, then `add the mushrooms`, let simmer for a couple of minutes while stirring occasionally
6. `Add all of the (uncooked) pasta`, then `add about ½L of broth`
   6.1 Place the lid on the pan, bring it to a boil, stir very frequently making sure the pasta does not get stuck on the bottom
   6.2 Whenever the pasta tends to get stuck to the bottom of the pan, `add some more broth` (I like to do this in batches of roughly `250-500mL`
   6.3 Keep stirring!
       * Out of broth? Check if the pasta is fully cooked (try different pieces!)
         **if so, go to the next step.
         **If not, add some more water if it is sticking too the pan too much. If it is not yet sticking very much, let it simmer some more - cook until pasta is cooked
7. `Turn off the heat` (it is fine if there is still some water in the pan), stir some more, then `stir in the crème fraîche`
8. Optionally make and add bacon
