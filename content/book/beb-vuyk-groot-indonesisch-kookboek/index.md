---
title: "Beb Vuyk - Groot Indonesisch Kookboek"
date: 2021-06-10T11:47:51+02:00
draft: false
categories: ["Indonesian"]
img: "beb_vuyk-groot_indonesisch_kookboek.jpg"
---

This book, literally translated "Great Indonesian Cookbook", is arguably the most
important book on Indonesian cuisine in the Netherlands. For over 30 years this
book, counting over 500 recipes, introduced the Dutch to authentic Indonesian
food. Still being printed and sold to this day, it is unlikely to be surpassed
any time soon.

Elizabeth (Beb) Vuyk (1905 – 1991) was a Dutch writer of Indo (Eurasian)
descent. She grew up in the Netherlands and went to Indonesia at the age of 24.
Three years later she married Fernand de Willigen, a native born Indo. They had
two sons, both born in the Dutch East Indies. She sympathised with the Indies
independence movement and during World War II she was captive in a Japanese
concentration camp.
