from slugify import slugify

# txt = "Southern German Potato Salad, Swabian Style (Schwäbische Kartoffelsalat)"
# r = slugify(txt)

txt = "Beetroot and Lamb's Lettuce Salad" # lamb's lettuce,[3] corn salad,[3] common cornsalad,[4] mâche[3] (/mɑːʃ/), fetticus,[3] feldsalat,[3] nut lettuce,[3] field salad.
# hugo new recipe/beetroot-and-lambs-lettuce-salad.md
#
r = slugify(txt)
print("hugo new recipe/{}.md".format(r))
