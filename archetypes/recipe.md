---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
tags: [] # ingredients, keywords
categories: [] # Breakfast, Lunch, Dinner, Snack, Soup, Drink, Pasta, Oven-dish
servings: "4"
lang: "en"
---

Description here

<!--more-->

# Notes
* `Serves 3-4`
* Contains `gluten` unless gluten-free pasta is used

# Ingredients
* `400-500g `
* `1 Onion`, finely minced
* `1½ to 2L of `
* `250mL `
* `1 tbsp of `
* `1 tsp of `

# Method
1. Item one

   Note:
2. Item two
   1. Subitem

      Note:

# Sources
* X, p. 376, recipe #32 (ISBN: )
